#
# Project makefile for a Tango project. You should normally only need to modify
# PROJECT below.
#

#
# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
PROJECT = ska-mid-talondx-fpgaregistermap

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
WEBJIVE ?= false# Enable Webjive

CI_PROJECT_PATH_SLUG ?= $(PROJECT)
CI_ENVIRONMENT_SLUG ?= $(PROJECT)
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gitlab_values.yaml)

# include Conan packages support
include .make/conan.mk

# include docs support
include .make/docs.mk

# include make support
include .make/make.mk

# include help support
include .make/help.mk

# include core release support
include .make/release.mk

CONAN_PROFILES ?= /usr/local/share/profiles
CONAN_X86_COMPILE_PROFILE ?= conan_x86_profile.txt
CONAN_AARCH64_COMPILE_PROFILE  ?= conan_aarch64_profile.txt

CONAN_COMPILE_USER ?= nrc
CONAN_COMPILE_CHANNEL ?= stable

BUILD_NATIVE = build-native
BUILD_CROSS  = build-cross
BUILD_TEST   = build-test

clean-native:
	rm -rf conan/$(PROJECT)/$(BUILD_NATIVE)

build-fpgaregistermap-native: ## Build the project under x86_64 architecture
	mkdir -p conan/$(PROJECT)/$(BUILD_NATIVE)
	cd conan/$(PROJECT)/$(BUILD_NATIVE); \
	  	cp $(CONAN_PROFILES)/$(CONAN_X86_COMPILE_PROFILE) .; \
	  	conan export .. $(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL); \
		conan install .. -s build_type=Release -pr $(CONAN_X86_COMPILE_PROFILE); \
		conan build ..; \
		make conan-do-package ..;
#		conan export-pkg -f .. $(PROJECT)/$(VERSION)@$(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL)
	
clean-cross:
	rm -rf conan/$(PROJECT)/$(BUILD_CROSS)

build-fpgaregistermap-cross: ## Build the project under aarch64 (armv8) architecture
	mkdir -p conan/$(PROJECT)/$(BUILD_CROSS)
	cd conan/$(PROJECT)/$(BUILD_CROSS); \
	  	cp $(CONAN_PROFILES)/$(CONAN_AARCH64_COMPILE_PROFILE) .; \
	  	conan export .. $(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL); \
		conan install .. -s build_type=Release -pr $(CONAN_AARCH64_COMPILE_PROFILE); \
		conan build ..; \
		conan package ..;
#		conan export-pkg -f .. $(PROJECT)/$(VERSION)@$(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL)

clean-test:
	rm -rf conan/$(PROJECT)/$(BUILD_TEST)

build-fpgaregistermap-test: ## Build the project under x86_64 architecture
	mkdir -p conan/$(PROJECT)/$(BUILD_TEST)
	cd conan/$(PROJECT)/$(BUILD_TEST); \
	  	cp $(CONAN_PROFILES)/$(CONAN_X86_COMPILE_PROFILE) .; \
	  	conan export .. $(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL); \
		conan install .. -s build_type=Debug -pr $(CONAN_X86_COMPILE_PROFILE); \
		conan build ..; \
		conan package ..;
#		conan export-pkg -f .. $(PROJECT)/$(VERSION)@$(CONAN_COMPILE_USER)/$(CONAN_COMPILE_CHANNEL)
