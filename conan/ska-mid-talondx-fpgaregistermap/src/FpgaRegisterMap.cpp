#include "FpgaRegisterMap.h"

/*****************************************************************************
 * FpgaRegisterMap CLASS Definition
 * This class provides the infrastructure to create and initialize memory maps
 * on the FPGA via the HPS across the Lightweight bridge.
 *****************************************************************************/

/** FpgaRegisterMap CONSTRUCTOR
 *
 * Method		: FpgaRegisterMap
 * Arguments	:
 * Description	:
 * Initializes an instance of the FpgaRegisterMap , with no parameters set.
 * The configure function is required prior to performing read/write functions
 * using this class, if constructing an object with this constructor.
 */
FpgaRegisterMap::FpgaRegisterMap()
{
}

/** FpgaRegisterMap CONSTRUCTOR
 *
 * Method		: FpgaRegisterMap
 * Arguments	:
 *    : param filename : The filename used to access the underlying memory map
 *    : type filename : string
 *    : param bridge_address : The base address for the bridge over which this
 *      memory map access the FPGA.
 *    : type bridge_address : uint32_t
 *    : param fw_offset : The offset from the bridge base address for the
 *      firmware register set being mapped.
 *    : type fw_offset : uint32_t
 *    : param mmap_size : The size in bytes for the underlying memory map
 *    : type mmap_size : uint32_t
 * Description	:
 * Initializes an instance of the FpgaRegisterMap class, with default variables.
 * The init map function is required prior to performing read/write functions
 * using this class.
 */
FpgaRegisterMap::FpgaRegisterMap(
	string filename,
	uint32_t bridge_address,
	uint32_t fw_offset,
	uint32_t mmap_size )
{
	configure( filename, bridge_address, fw_offset, mmap_size );
}

/** configure
 *
 * Method      : configure
 * Arguments   :
 *    : param filename : The filename used to access the underlying memory map
 *    : type filename : string
 *    : param bridge_address : The base address for the bridge over which this
 *      memory map access the FPGA.
 *    : type bridge_address : uint32_t
 *    : param fw_offset : The offset from the bridge base address for the
 *      firmware register set being mapped.
 *    : type fw_offset : uint32_t
 *    : param mmap_size : The size in bytes for the underlying memory map
 *    : type mmap_size : uint32_t
 * Description:
 * 	    Initializes the parameters of an FpgaRegisterMap object, then calls the
 *      init_map function to initialize the map.
 */
void FpgaRegisterMap::configure(
	string filename,
	uint32_t bridge_address,
	uint32_t fw_offset,
	uint32_t mmap_size )
{
	set_filename( filename );
	set_bridge_address( bridge_address );
	set_fw_offset( fw_offset );
	set_mmap_size( mmap_size );

	init_map();
}

/** set_filename
 *
 * Method		: set_filename
 * Arguments	:
 *    : param set : The filename used to access the underlying memory map
 *    : type set : string
 * Description	:
 *      Initializes the class variable 'filename' to the provided string value.
 */
void FpgaRegisterMap::set_filename( string set )
{
	filename_ = set;
	DEBUG_MSG( cout, "Set filename_ to [" << filename_ << "]" );
}

/** set_bridge_address
 *
 * Method		: set_bridge_address
 * Arguments	:
 *    : param set : The 32-bit base address of the bridge used to access the
 *      underlying memory map. The value provided should align to a 4K page.
 *    : type set : uint32_t
 * Description	:
 * Initializes the class variable 'base_address' to the provided uint32_t value.
 */
void FpgaRegisterMap::set_bridge_address( uint32_t set )
{
	bridge_address_ = set;
	DEBUG_MSG( cout, "Set bridge_address_ to [" << bridge_address_ << "]" );
}

/** set_fw_offset
 *
 * Method		: set_fw_offset
 * Arguments	:
 *    : param set : The 32-bit offset from the bridge_address used to access
 *      the underlying memory map
 *    : type set : uint32_t
 * Description	:
 *      Initializes the class variable 'fw_offset' to the provided uint32_t
 *      value.
 */
void FpgaRegisterMap::set_fw_offset( uint32_t set )
{
	fw_offset_ = set;
	DEBUG_MSG( cout, "Set fw_offset_ to [" << fw_offset_ << "]" );
}

/** set_mmap_size
 *
 * Method		: set_mmap_size
 * Arguments	:
 *    : param set : The 32-bit memory map size value used to describe the
 *      extent of the underlying memory map
 *    : type set : uint32_t
 * Description	:
 *      Initializes the class variable 'mmap_size' to the provided uint32_t
 *      value (specified in number of bytes).
 */
void FpgaRegisterMap::set_mmap_size( uint32_t set )
{
	mmap_size_ = set;
	DEBUG_MSG( cout, "Set mmap_size_ to [" << mmap_size_ << "]" );
}

/** is_open
 *
 * Method		: is_open
 * Arguments	:
 *    : return bool : A boolean indicating whether the memory map represented
 *      by the instance of this class is open for read/write transactions.
 * Description	:
 *      This functions returns true if the file descriptor of the underlying
 *      memory map is 0 or greater, false otherwise.
 */
bool FpgaRegisterMap::is_open()
{
	return fd_ >= 0;
}

/** init_map
 *
 * Method		: init_map
 * Arguments	:
 *    : return int : Returns an integer indication of success upon completion
 *      of the initialization task.
 * Description	:
 *      Opens a file descriptor to the configured filename, and attempts to
 *      create a memory map given the base address and size parameters supplied.
 *      If the parameters are not set, or are incorrect, the operation fails
 *      and returns -1. Upon success, the operation returns 0.
 */
void FpgaRegisterMap::init_map()
{

	// Calculate the rounding_offset. This is the number of bytes required to
	// align the memory map base address to a 4K page boundary

	alignment_offset_ = ( bridge_address_ + fw_offset_ ) % PAGE_SIZE;

	DEBUG_VAR( cout, mmap_size_ );
	DEBUG_VAR( cout, bridge_address_ );
	DEBUG_VAR( cout, fw_offset_ );
	DEBUG_VAR( cout, alignment_offset_ );

	fd_ = open( filename_.c_str(), O_RDWR | O_SYNC );

	if ( fd_ < 0 )
	{
		fd_ = 0;
		ERROR_MSG( cerr, "Failed to open file : ERRNO=" << strerror( errno ) );
        throw std::system_error(errno, std::generic_category());
		return;
	}

	try {
		map_ = mmap( NULL,
			mmap_size_ + alignment_offset_,
			PROT_WRITE | PROT_READ,
			MAP_SHARED,
			fd_,
			bridge_address_ + fw_offset_ - alignment_offset_ );
	}
	catch (std::exception& e)
	{
		ERROR_MSG( cerr, "Failed to initialize memory map. "
										 << strerror( errno ) << " " << e.what() );
		close( fd_ );
        throw std::system_error(errno, std::generic_category());
		return;
	}

	// add the alignment offset back to the mmap pointer to align to the start
	// of the register set
	map_ += alignment_offset_;

	DEBUG_VAR( cout, fd_ );
	DEBUG_VAR( cout, map_ );
	DEBUG_VAR( cout, mmap_size_ );

}

/** close_map
 *
 * Method		: close_map
 * Arguments	:
 *    : return int : Returns an integer indication of success upon completion
 *      of the close task.
 * Description	:
 *      If the class represents an active (open) bridge, this function unmaps
 *      the memory map, and subsequently closes the file associated with the
 *      map. Upon success, the operation returns 0, and returns -1 if an error
 *      occurs.	At the entrance to this method, the assertion is made that the
 *      file descriptor must be open prior to proceeding.
 */
void FpgaRegisterMap::close_map()
{

	DEBUG_VAR( cout, mmap_size_ );
	DEBUG_VAR( cout, alignment_offset_ );

	assert( is_open() );

	map_ -= alignment_offset_;
	int unmap = munmap( map_, this->mmap_size_ + this->alignment_offset_ );

	DEBUG_VAR( cout, unmap );
	if ( unmap < 0 )
	{
		ERROR_MSG( cerr, "Error in munmap call, returns "
										 << unmap << " : " << strerror( errno ) );
    	throw std::system_error(errno, std::generic_category());
	}

	if ( close( fd_ ) < 0 )
	{
		ERROR_MSG( cerr, "Error in close call : " << strerror( errno ) );
		throw std::system_error(errno, std::generic_category());
	}

}


/** sync_map
 *
 * Method		: sync_map
 * Arguments	: none
 * Description	:
 *      Synchronize the memory map with reads and writes by invoking the
 *      msync call.
 */
int FpgaRegisterMap::sync_map_()
{
	DEBUG_MSG( cout, "sync_map_ called." );

	return msync(
		map_ - alignment_offset_,
		mmap_size_ + alignment_offset_,
		MS_SYNC );
}
