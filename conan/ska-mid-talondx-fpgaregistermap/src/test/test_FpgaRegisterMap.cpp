#include "gtest/gtest.h"
#include "FpgaRegisterMap.h"
#include "test.h"

#define TEST_REPEAT 3


class FpgaRegisterMapTest : public ::testing::Test {

protected:

  const string filename = "FpgaRegisterMap_test_binary_file.bin";
  uint64_t test_contents[TEST_SIZE];
  FpgaRegisterMap map;

  /*******************************************************************************
   *
   * remove_test_tile
   *
   * Deletes the binary test file that was generated at start.
   ******************************************************************************/
  void remove_test_file()
  {
    string filename = TEST_FILENAME;
    if ( remove( filename.c_str() ) != 0 )
    {
      TEST_ERROR_MESSAGE( cout, "Error deleting test file." );
      perror( "Error deleting test file." );
    }
  }

  int create_test_file()
  {
    for ( int i=0; i<TEST_SIZE; i++ )
      test_contents[i] = (uint64_t) ( rand() % 0xffffffff ) + ( (uint64_t) ( rand() % 0xffffffff ) << 32 );

      ofstream testfile;
      testfile.open( TEST_FILENAME, ios::out | ios::ate | ios::binary );
      testfile.seekp( 0 );
      if ( testfile.is_open() )
      {
        testfile.write( (const char*)test_contents, TEST_SIZE*8 );
      } else
      {
        testfile.close();
        TEST_ERROR_MESSAGE( cout, "Unable to open file for writing." );
        return -1;
      }

      testfile.close();
      return 1;
  }

  void SetUp() override
  {
    srand( time(NULL) );
    if ( create_test_file() > 0 )
      map.configure( TEST_FILENAME, 0, 0, TEST_SIZE*4 );
    else
      cout << "test file not generated" << endl;
  }

  void TearDown() override
  {
    using namespace ::testing;
    remove_test_file();
  }

  template<typename T>
  int random_offset( T var )
  {
    // generate random offset in byte-space of supplied type size
    int offset = rand() % ( TEST_SIZE * sizeof(long)/sizeof(T) );

    // byte align random offset
    int x = ALIGN( offset, sizeof( T ) );
    offset = x > sizeof( T ) ? x - sizeof( T ) : x;
    return offset;
  }

  template<typename T>
  T orig_value( int offset, T mask )
  {
    // Calculate the array index of the corresponding 64-bit original array of values
    int offset64 = offset/sizeof(uint64_t);
    uint64_t orig_value64 = test_contents[offset64];

    int residoffset = ( offset % sizeof(uint64_t) ) / sizeof(T);
    //TEST_VALUE( cout, "Residual offset within 64-bit number (in " << sizeof(T)*BYTE_SIZE << "-bit chunks)", residoffset );
    T orig_value = orig_value64 >> ( residoffset * BYTE_SIZE*sizeof(T) ) & mask;
    return orig_value;
  }

  template<typename T>
  static T byte_mask( T value )
  {
    return (T) ( 1 << BYTE_SIZE*sizeof(T) ) - 1;
  }

};



#include "test_FpgaRegisterMap_fileread.h"
#include "test_FpgaRegisterMap_register_rw.h"
#include "test_FpgaRegisterMap_registerfield_rw.h"
