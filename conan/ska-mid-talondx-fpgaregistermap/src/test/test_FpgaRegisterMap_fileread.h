using namespace ::testing;

TEST_F( FpgaRegisterMapTest, FileReadTest ) {

  uint64_t value64 = 0;
  for ( int i=0; i<TEST_SIZE; i++ )
  {
    map.FpgaRegisterMap::read_register( i*sizeof(uint64_t), &value64 );
    EXPECT_EQ( test_contents[i], value64 );
  }

}
