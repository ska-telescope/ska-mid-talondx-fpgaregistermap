#include "gtest/gtest.h"
#include "FpgaRegisterMap.h"
#include "test.h"

int   t_argc;
char *t_argv[32];
int   t_result = 1;

bool do_tests()
{
  ::testing::InitGoogleTest( &t_argc, t_argv );
  t_result = RUN_ALL_TESTS();

  return true | ( t_result == 0 );
}

int main( int argc, char **argv )
{

  t_argc = argc;
  memcpy( t_argv, argv, (argc+1) * sizeof( char* ));

  cout << "in main, running tests" << endl;

  do_tests();
  return( t_result );


}
