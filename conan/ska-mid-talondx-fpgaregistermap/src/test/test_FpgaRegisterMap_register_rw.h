using testing::Types;

template<typename T>
class FpgaRegisterMap_RegisterRW_Test : public FpgaRegisterMapTest {
protected:
  FpgaRegisterMap_RegisterRW_Test() {}
  virtual ~FpgaRegisterMap_RegisterRW_Test() {}
};

TYPED_TEST_CASE_P( FpgaRegisterMap_RegisterRW_Test );

TYPED_TEST_P( FpgaRegisterMap_RegisterRW_Test, UnsignedValueTest )
{
  TypeParam orig_test_value, orig_register_value,
            new_test_value, new_register_value,
            verify, mask;

  mask = FpgaRegisterMapTest::byte_mask( mask );

  for ( int i=0; i<TEST_REPEAT; i++ )
  {
    // this is a register offset in the byte-space of the register size.
    int offset = FpgaRegisterMapTest::random_offset( orig_test_value );
    orig_test_value = FpgaRegisterMapTest::orig_value( offset, mask );

    FpgaRegisterMapTest::map.read_register( offset, &orig_register_value );

    EXPECT_EQ( orig_test_value, orig_register_value );

    new_test_value = rand() % mask;

    FpgaRegisterMapTest::map.write_register( offset, new_test_value );
    FpgaRegisterMapTest::map.read_register( offset, &new_register_value );

    EXPECT_EQ( new_test_value, new_register_value );

    FpgaRegisterMapTest::map.write_register( offset, orig_register_value );
    FpgaRegisterMapTest::map.read_register( offset, &verify );

    EXPECT_EQ( orig_register_value, verify );
  }

}

REGISTER_TYPED_TEST_CASE_P( FpgaRegisterMap_RegisterRW_Test, UnsignedValueTest );

typedef Types< int8_t, uint8_t,
               int16_t, uint16_t,
               int32_t, uint32_t,
               int64_t, uint64_t > FpgaRegisterMapDataTypes;

INSTANTIATE_TYPED_TEST_CASE_P( FpgaRegisterMapUnitTesting,
                               FpgaRegisterMap_RegisterRW_Test,
                               FpgaRegisterMapDataTypes );
