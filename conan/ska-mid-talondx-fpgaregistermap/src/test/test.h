#include <unistd.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <chrono>

#define GT "\033[1;32m"
#define RT "\033[1;31m"
#define ET "\033[0m"

#define DEBUG

#define TEST_MAJOR_MESSAGE( os, msg ) \
  (os) << "[==========] " << msg << endl

#define TEST_MINOR_MESSAGE( os, msg ) \
  (os) << "[----------] " << msg << endl;

#define TEST_MESSAGE( os, msg ) \
  (os) << GT << "[          ] " << ET << msg << endl;

#define TEST_PASSED_MESSAGE( os, msg ) \
  (os) << GT << "[  PASSED  ] " << ET << msg << endl

#define TEST_FAILED_MESSAGE( os, msg ) \
  (os) << RT << "[  FAILED  ] " << ET << msg << endl

#define TEST_ERROR_MESSAGE( os, msg ) \
  (os) << RT << "[ ERROR    ] " << ET << msg << endl

#define TEST_START( os, num, name ) \
  (os) << GT << "[ RUN      ] " << ET << name << " : START " << endl

#define TEST_END_REPORT( os, reps, pass, fail, name ) \
  (os) << GT << "[          ] " << ET << name << " : TEST REPORT " << endl \
       << GT << "[          ] " << ET << "  Ran " << reps << " iterations " << endl \
       << GT << "[          ] " << ET << "  # Successful   [ " << GT << pass << ET << " / " << reps << " ]" << endl \
       << GT << "[          ] " << ET << "  # Unsuccessful [ " << ( fail > 0 ? "\033[1;31m" : "" ) << fail \
       << ( fail > 0 ? "\033[0m" : "" ) << " / " << reps << " ]" << endl

#define TEST_END_SUCCESS( os, name, duration ) \
  (os) << GT << "[       OK ] " << ET << name << " (" << duration << "ms)"<< endl

#define TEST_END_FAILURE( os, name, msg ) \
  (os) << "FpgaRegisterMap Test [" << name << "]: END ---- " \
       << "\033[1;31mFAILED\033[0m" << endl << "   Reason: \033[1;31m" \
       << msg << "\033[0m"<< endl

#ifdef DEBUG
#define TEST_VALUE( os, name, value ) \
  (os) << "   " << name << " : " << value << endl
#else
#define TEST_VALUE( os, name, value )
#endif

#ifdef DEBUG
#define TEST_VALUE_HEX( os, name, value ) \
  (os) << "   " << name << " : 0x" << setfill('0') << setw(sizeof(value)*2) \
       << hex << (uint64_t) value << dec \
       << " (" << (int64_t) value << ") " << endl
#else
#define TEST_VALUE_HEX( os, name, value )
#endif

#ifdef DEBUG
#define TEST_VALUE_HEX_BIN( os, name, value, binval ) \
  (os) << "   " << name << " : 0x" << setfill('0') << setw(sizeof(value)*2) \
       << hex << (uint64_t) value << dec << " 0b" << binval << endl
#else
#define TEST_VALUE_HEX_BIN( os, name, value, binval )
#endif

#ifdef DEBUG
#define TEST_REG_VALUE( os, msg, offset, value ) \
  (os) << "   " << msg << " [" << dec << offset << "] : 0x" << setfill('0') \
       << setw(sizeof(value)*2) << hex << (uint64_t) value << dec << endl
#else
#define TEST_REG_VALUE( os, msg, offset, value )
#endif

#ifdef DEBUG
#define TEST_VALUE_BIN( os, name, offset, value ) \
  (os) << "   " << name << " [" << offset << "] : 0b" << value << endl
#else
#define TEST_VALUE_BIN( os, name, offset, value )
#endif

#ifdef DEBUG
#define TEST_MSG( os, msg ) \
  (os) << "   " << msg << endl
#else
#define TEST_MSG( os, msg )
#endif

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define BYTE_SIZE 8
#define TEST_SIZE 100
#define TEST_REPEATS 10

#define TEST_FILENAME "test_FpgaRegisterMap.bin"

using ValueArray = uint64_t[TEST_SIZE];
