using testing::Types;

template<typename T>
class FpgaRegisterMap_RegisterFieldRW_Test : public FpgaRegisterMapTest {
protected:
  FpgaRegisterMap_RegisterFieldRW_Test() {}
  virtual ~FpgaRegisterMap_RegisterFieldRW_Test() {}
};

TYPED_TEST_CASE_P( FpgaRegisterMap_RegisterFieldRW_Test );

TYPED_TEST_P( FpgaRegisterMap_RegisterFieldRW_Test, UnsignedValueTest )
{
  TypeParam orig_test_value, orig_register_value,
            new_test_value, new_register_value,
            verify, reg_verify, mask,
            field_test_value, orig_field_value,
            new_field_test_value, new_field_value,
            field_verify, field_mask,
            orig_verify;

  mask = FpgaRegisterMapTest::byte_mask( mask );

  for ( int i=0; i<TEST_REPEAT; i++ )
  {
    // this is a register offset in the byte-space of the register size.
    int offset = FpgaRegisterMapTest::random_offset( orig_test_value );
    orig_test_value = FpgaRegisterMapTest::orig_value( offset, mask );

    FpgaRegisterMapTest::map.read_register( offset, &orig_register_value );

    // Test that register value read from file is equal to the value from the test array
    EXPECT_EQ( orig_test_value, orig_register_value );

    int bitwidth = rand() % ( BYTE_SIZE*sizeof(TypeParam) - 1 ) + 1;
    int bitoffset = rand() % ( BYTE_SIZE*sizeof(TypeParam) - bitwidth );
    field_mask = ( (TypeParam) 1 << bitwidth ) - 1;
    field_test_value = rand() % ( field_mask );

    // Read original field value from file
    FpgaRegisterMapTest::map.read_register_field( offset, bitoffset, bitwidth, &orig_field_value );
    // Write new test field value to file
    FpgaRegisterMapTest::map.write_register_field( offset, bitoffset, bitwidth, field_test_value, orig_register_value );
    // Read field value from file - should be test value
    FpgaRegisterMapTest::map.read_register_field( offset, bitoffset, bitwidth, &field_verify );
    // Read register value from file
    FpgaRegisterMapTest::map.read_register( offset, &reg_verify );
    // Write original field value back to file
    FpgaRegisterMapTest::map.write_register_field( offset, bitoffset, bitwidth, orig_field_value, orig_register_value );
    // Read register value from file
    FpgaRegisterMapTest::map.read_register( offset, &verify );

    // Test that we have the original test value back in the file
    EXPECT_EQ( verify, orig_test_value );
    // Test that the field written, is equal to the field subsequently read.
    EXPECT_EQ( field_verify, field_test_value );

    // Transform the original test value from the array in the same manner and compared
    orig_verify = ( orig_test_value & ~( field_mask << bitoffset ) ) |
                  ( ( (TypeParam) field_test_value << bitoffset ) & ( field_mask << bitoffset ) );
    EXPECT_EQ( orig_verify, reg_verify );



  }

}

REGISTER_TYPED_TEST_CASE_P( FpgaRegisterMap_RegisterFieldRW_Test, UnsignedValueTest );

typedef Types< int8_t, uint8_t,
               int16_t, uint16_t,
               int32_t, uint32_t,
               int64_t, uint64_t > FpgaRegisterMapDataTypes;

INSTANTIATE_TYPED_TEST_CASE_P( FpgaRegisterMapUnitTesting,
                               FpgaRegisterMap_RegisterFieldRW_Test,
                               FpgaRegisterMapDataTypes );
