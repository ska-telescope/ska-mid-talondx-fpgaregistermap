#ifndef FPGA_REGISTER_MAP_H
#define FPGA_REGISTER_MAP_H

// Uncomment this line to remove the assertion processing.
//#define NDEBUG

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <cstdint>
#include <iostream>
#include <sys/mman.h>
#include <exception>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <memory>

using namespace std;

//#define DEBUG

// Define debug output stream macros to provide messaging in DEBUG mode
#ifdef DEBUG
#define DEBUG_VAR( os, var ) \
  (os) << "FpgaRegisterMap::" << __func__ << "(" << __LINE__ << ")  " \
       << #var << " = [" << (var) << "]" << endl
#else
#define DEBUG_VAR( os, var )
#endif

#ifdef DEBUG
#define DEBUG_VAR_HEX( os, var ) \
  (os) << "FpgaRegisterMap::" << __func__ << "(" << __LINE__ << ")  " \
       << #var << " = [" << hex << (var) << dec << "]" << endl
#else
#define DEBUG_VAR_HEX( os, var )
#endif

#ifdef DEBUG
#define DEBUG_MSG( os, msg ) \
  (os) << "FpgaRegisterMap::" << __func__ << "(" << __LINE__ << ") " \
       << msg << endl
#else
#define DEBUG_MSG( os, msg )
#endif

#define ERROR_MSG( os, msg ) \
  (os) << "FpgaRegisterMap::" << __func__ << "(" << __LINE__ << ") " \
       << msg << endl

// Page Size used to calculate alignment offset of the memory map
#define PAGE_SIZE 4096

//	Additional Class Declarations
/**
 * The fpga_register_map class is used to instantiate a register map object to
 * allow derived classes to efficiently establish memory maps via the FPGA
 * bridge(s) and facilitate read and write from and to registers.
 */
class FpgaRegisterMap : public exception
{

public:

	FpgaRegisterMap();

  FpgaRegisterMap(
    string filename,
    uint32_t bridge_address,
    uint32_t fw_offset,
    uint32_t mmap_size );

	virtual ~FpgaRegisterMap()
	{
		map_ = NULL;
	}

  /** Virtual public function FpgaRegisterMap_create
   *
   * Method  : FpgaRegisterMap_create
   * Arguments  : none
   * Description  : 
   *      Returns a shared_ptr<FpgaRegisterMap> object to the caller.
   *      This function is required in order to allow the created FpgaRegisterMap
   *      object to be mocked during testing.
   */
  virtual std::shared_ptr<FpgaRegisterMap> FpgaRegisterMap_create()
  {
    return std::make_shared<FpgaRegisterMap>();
  }

	virtual void configure(
    string filename,
    uint32_t bridge_address,
    uint32_t fw_offset,
    uint32_t mmap_size );

	virtual void init_map();

  virtual void close_map();

  /** Public function read_register
   *
   * Method		: read_register
   * Arguments	:
   *    : param offset : The register address offset from the base, expressed
   *      in bytes.
   *	  : type offset : uint32_t
   *    : param *value : A pointer for the return value. This argument is
   *      templated to support multiple data types
   *    : type *value : template <typename T>
   *    : param sign_extend : An optional flag indicating the signed-ness of
   *      the desired return variable. If set to TRUE, the return value will
   *      have a sign_extend operation performed, returning an integer.
   *      Default is FALSE.
   *    : type sign_extend : bool
   * Description	:
   *      Reads from the specified offset in the memory map, and
   *      assigns the supplied pointer to the read value. By default this
   *      function returns an unsigned value, provide a TRUE argument for sign_
   *      extend to handle integer numbers.
   */
  template <typename T>
  void read_register(
    uint32_t offset,
    T *value,
    bool sign_extend=false )
  {
  	read_register_( offset, value );
  	if ( sign_extend )
  		if ( *value >> ( ( sizeof( T ) * 8 ) - 1 ) & 0x1 )
  			sign_extend_( value, sizeof( T ) * 8 );
  }

  /** Public function read_register_field
   *
   * Method		: read_register_field
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type offset : uint32_t
   *    : param bit_offset : The bit offset of the register field, as measured
   *      from the LSB 0
   *    : type bit_offset : uint32_t
   *    : param bit_width : The width of the field to be read, as measured in
   *      bits
   *    : type bit_width : uint32_t
   *    : param *value : A pointer for the return value. This argument is
   *      templated to support multiple data types
   *    : type *value : template <typename T>
   *    : param sign_extend : An optional flag indicating the signed-ness of
   *      the desired return variable. If set to TRUE, the return value will
   *      have a sign_extend operation performed, returning an integer.
   *      Default is FALSE.
   *    : type sign_extend : bool
   * Description	:
   *      Reads from the specified offset in the memory map, mask the returned
   *      value as specified, and assigns the supplied pointer to the resulting
   *      value. By default this function returns an unsigned value, provide
   *      a TRUE argument for sign_extend to handle integer numbers.
   */
  template <typename T>
  void read_register_field(
    uint32_t register_offset,
    uint32_t bit_offset,
    uint32_t bit_width,
    T *value,
    bool sign_extend=false )
  {
  	read_register_field_( register_offset, bit_offset, bit_width, value );
  	if ( sign_extend )
  		if ( ( ( sizeof( T ) * 8 ) > bit_width ) &&
           ( ( *value >> bit_width-1 ) & 0x1 ) )
  			sign_extend_( value, bit_width );
  }

  /** Public function write_register
   *
   * Method		: write_register
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type register_offset : uint32_t
   *    : param data : The data to be written to the register address. This
   *      argument is templated to support multiple data types.
   *    : type data : template <typename T>
   * Description	:
   *      Write the provided value to the specified register offset.
   */
  template <typename T>
  void write_register(
    uint32_t register_offset,
    T data )
  {
  	write_register_( register_offset, data );
  }

  /** Public function write_register_field
   *
   * Method		: write_register_field
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type offset : uint32_t
   *    : param bit_offset : The bit offset of the register field, as measured
   *      from the LSB 0
   *    : type bit_offset : uint32_t
   *    : param bit_width : The width of the field to be read, as measured
   *      in bits
   *    : type bit_width : uint32_t
   *    : param data : The data to be written. This argument is templated to
   *      support multiple data types.
   *    : type data : template <typename T>
   *    : param reg_data : The data value corresponding to the value of the
   *      register containing the field to be written prior to the write. This
   *      template is distinct from the data value supplied to allow for
   *      different register sizes to be written under the DeTri system,
   *      as well as to accommodate shadow register implementation.
   *    : type reg_data : template <typename U>
   * Description	:
   *      Reads the register, masks the bits according to specified values,
   *      modifies the register value according to the supplied data, then
   *      writes the register.
   */
  template <typename T, typename U>
  void write_register_field(
    uint32_t register_offset,
    uint32_t bit_offset,
    uint32_t bit_width,
    T data,
    U register_data )
  {
  	write_register_field_( register_offset, bit_offset, bit_width,
                           data, register_data );
  }

/*******************************************************************************
 *
 * PRIVATE class functions
 *
 ******************************************************************************/

private:

  /* Holds the file descriptor for the register map */
	int fd_ = 0;
  /* Pointer to the map object */
	void* map_;
  /* Filename used to refer to the map location */
	string filename_ = "";
  /* Bridge address offset (bytes) */
	uint32_t bridge_address_ = 0;
  /* Alignment offset (rounding offset) (bytes) the number of bytes required to
     align memory map base address to a 4k page boundary. */
	uint32_t alignment_offset_ = 0;
  /* Firmware offset (bytes) the offset from the bridge address */
	uint32_t fw_offset_ = 0;
  /* size of the map in bytes */
	uint32_t mmap_size_ = 0;

	void set_bridge_address( uint32_t set );
	void set_filename( string set );
	void set_fw_offset( uint32_t set );
	void set_mmap_size( uint32_t set );

	bool is_open();
	int sync_map_();

  /** Private function read_register_
   *
   * Method		: read_register_
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type register_offset : uint32_t
   *    : param *value : A pointer for the return value. This argument is
   *      templated to support multiple data types
   *    : type *value : template <typename T>
   * Description	:
   *      Reads from the specified offset in the memory map, and assigns the
   *      supplied pointer to the read value. At the entrance to this method,
   *      the assertion is made that the file descriptor must be open prior to
   *      proceeding.
   */
  template<typename T>
  void read_register_(
    uint32_t register_offset,
    T* value )
  {
  	DEBUG_MSG( cout, "read_register_" );
  	DEBUG_VAR ( cout, register_offset );

  	assert( is_open() );
  	uint32_t reg_offset = register_offset;
  	T *reg;

  	reg = (T*) map_;
  	reg += reg_offset / sizeof( T );

  	*value = *reg;

  	DEBUG_VAR ( cout, register_offset );
  	DEBUG_VAR_HEX ( cout, *value );

  }

  /** Private function read_register_field_
   *
   * Method		: read_register_field_
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type offset : uint32_t
   *    : param bit_offset : The bit offset of the register field, as measured
   *      from the LSB 0
   *    : type bit_offset : uint32_t
   *    : param bit_width : The width of the field to be read, as measured
   *      in bits
   *    : type bit_width : uint32_t
   *    : param *value : A pointer for the return value. This argument is
   *      templated to support multiple data types
   *    : type *value : template <typename T>
   * Description	:
   *      Reads from the specified offset in the memory map, mask the returned
   *      value as specified, and assigns the supplied pointer to the resulting
   *      value. At the entrance to this method, the assertion is made that the
   *      file descriptor must be open prior to proceeding.
   */
  template <typename T>
  void read_register_field_(
    uint32_t register_offset,
    uint32_t bit_offset,
    uint32_t bit_width,
    T *value,
    int size=32 )
  {
  	DEBUG_MSG( cout, "read_register_field_" );
  	DEBUG_VAR( cout, register_offset );
  	DEBUG_VAR( cout, bit_offset );
  	DEBUG_VAR( cout, bit_width );
  	DEBUG_VAR( cout, size );

  	assert( is_open() );

  	T *reg;

  	reg = (T*) map_;
  	reg += register_offset / sizeof( T );
  	*value = (T) ( *reg >> bit_offset ) & calc_mask_( (T) bit_width );

  	DEBUG_VAR( cout, sizeof(*value));
  	DEBUG_VAR ( cout, *value );

  }

  /** Private function write_register_
   *
   * Method		: write_register_
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type register_offset : uint32_t
   *    : param data : The data to be written to the register address. This
   *      argument is templated to support multiple data types.
   *    : type data : template <typename T>
   * Description	:
   *      Write the provided value to the specified register offset. At the
   *      entrance to this method, the assertion is made that the file
   *      descriptor must be open prior to proceeding.
   */
  template<typename T>
  void write_register_(
    uint32_t register_offset,
    T data )
  {
  	DEBUG_MSG( cout, "write_register_" );
  	DEBUG_VAR( cout, register_offset );
  	DEBUG_VAR( cout, data );

  	assert( is_open() );

  	T *reg;

  	reg = (T *) map_ + ( register_offset / sizeof(T) );
  	*reg = data;

  	DEBUG_VAR( cout, *reg );

  }

  /** Private function write_register_field_
   *
   * Method		: write_register_field_
   * Arguments	:
   *    : param register_offset : The register address offset from the base,
   *      expressed in bytes.
   *	  : type offset : uint32_t
   *    : param bit_offset : The bit offset of the register field, as measured
   *      from the LSB 0
   *    : type bit_offset : uint32_t
   *    : param bit_width : The width of the field to be read, as measured
   *      in bits
   *    : type bit_width : uint32_t
   *    : param data : The data to be written. This argument is templated to
   *      support multiple data types.
   *    : type data : template <typename T>
   *    : param reg_data : The data value corresponding to the value of the
   *      register containing the field to be written prior to the write. This
   *      template is distinct from the data value supplied to allow for
   *      different register sizes to be written under the DeTri system, as
   *      well as to accommodate shadow register implementation.
   *    : type reg_data : template <typename U>
   * Description	:
   *      Reads the register, masks the bits according to specified values,
   *      modifies the register value according to the supplied data, then
   *      writes the register. At the entrance to this method, the assertion is
   *      made that the file descriptor must be open prior to proceeding.
   */
  template <typename T, typename U>
  void write_register_field_(
    uint32_t register_offset,
    uint32_t bit_offset,
    uint32_t bit_width,
    T data,
    U reg_data )
  {
  	DEBUG_MSG( cout, "write_register_field_" );
  	DEBUG_VAR( cout, register_offset );
  	DEBUG_VAR( cout, bit_offset );
  	DEBUG_VAR( cout, bit_width );
  	DEBUG_VAR( cout, data );
  	DEBUG_VAR( cout, reg_data );

  	assert( is_open() );

  	U mask = calc_mask_( (U) bit_width ) << bit_offset;
    U new_register_data =
  		(   reg_data		     & ~mask ) |
  		( ( (U) data << bit_offset ) &  mask );

  	write_register_( register_offset, new_register_data );

  	DEBUG_VAR( cout, new_register_data );

  }

  /** Private function calc_mask_
   *
   * Method		: calc_mask_
   * Arguments	:
   *    : param width : The bit width of the mask.
   *    : type width : template <typename T>
   *    : return : Returns a bit mask for the specified width in the templated
   *      type.
   *    : type return : template <typename T>
   * Description	:
   *      Creates and returns a bitmask of the specified width. The return
   *      value is the same type as the supplied width argument value.
   */
  template<typename T>
	static inline T calc_mask_( T width )
	{
		return ( (T) 1 << width ) - 1;
	}

  /** Private function sign_extend_
   *
   * Method		: sign_extend_
   * Arguments	:
   *    : param value : A pointer to the value.
   *    : type value : template <typename T>*
   *    : param width : The width of the value on which the sign extension is
   *      applied, in bits.
   *    : type width : uint32_t
   * Description	:
   *      Performs a sign extension operation on the value to which the
   *      supplied pointer points. The purpose of this operation is to
   *      interpret an unsigned value as an integer. Points the supplied
   *      pointer to the new integer value.
   */
	template <typename T>
	static inline void sign_extend_( T* value, uint32_t width )
	{
		T retval = -1;
		*value = ( retval &= ~calc_mask_( width ) ) |= *value;
	}

};

#endif // FPGA_REGISTER_MAP_H
