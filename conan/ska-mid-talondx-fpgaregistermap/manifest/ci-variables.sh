#!/bin/bash

readarray -t arr < "manifest/MANIFEST.skao.int.in"
for i in "${arr[@]}"
do
  if [[ $i == CI_* ]] || [[ $i == GITLAB* ]] ;
  then   
    sed -i s"^$i^$i=${!i}^" "MANIFEST.skao.int"
  fi
done
