from conans import ConanFile, CMake, tools
from conans.tools import load
import re
import os

def get_version():
    # This method opens the .release file that is required to be within the
    # project top-level directory in the repository.
    # The file should contain as a first line, the text:
    #
    # release={major}.{minor}.{patch}
    #
    # where major, minor and patch are integers

    try:
        content = load( "../../.release" )
        return re.search( "release=([0-9.]*)", content ).group(1)
    except Exception as e:
        return None

class FpgaregistermapConan(ConanFile):

    # The name to be given to the main target/artifact built by this repository
    name = "ska-mid-talondx-fpgaregistermap"

    # Call the version method to extract version information from the VERSION.txt
    # file within the repo
    version = get_version()

    # The standard SKA license clause. Contents of the license are contained in the
    # LICENSE file within the repo
    license = "BSD-3-Clause"

    # Name and email of the author
    author = "Dave Del Rizzo (Dave.DelRizzo@nrc-cnrc.gc.ca)"

    source_git_url = "gitlab.com/ska-telescope/"

    # set source_git_path to the intervening path within gitlab to the repository
    source_git_path = ""

    # set the source_git_name to the gitlab project slug. This is normally equivalent
    # to the name variable defined above
    source_git_name = name

    # Set source_folder_branch to the name of the git repository branch (default=main)
    source_folder_branch = "main"

    # Obtain a CI_JOB_TOKEN from the system environment.
    # Note that this should return an empty value if not running in gitlab CI
    if ( "CI_JOB_TOKEN" in os.environ ):
        ci_job_token = os.getenv('CI_JOB_TOKEN') + "@"
    else:
        ci_job_token = ""

    # The gitlab URL for the repository. Note that the git project stub is the
    # same as the defined source_folder.
    url = f"https://{ ci_job_token }{source_git_url}{ source_git_path }{ source_git_name }.git"

    # A brief description of the package
    description = "The FPGA Register Map class provides the infrastructure for TANGO devices to access register values on the FPGA via either High Performance or Lightweight bridge from HPS to the FPGA. The class is written in C++."

    # Topics are keywords for this package
    topics = ( "FPGA Register Access",
               "registerDef",
               "TANGO Device Server"
               )

    # Establish setting key-value pairs to describe possible configurations of
    # this package
    settings = { "os": ["Linux"],
                 "compiler" : [ "gcc" ],
                 "build_type": [ "Debug", "Release" ],
                 "arch" : [ "x86", "x86_64", "armv8" ]
                 }

    # Establish option key-value pairs to describe possible option configurations
    # for this package
    options = {"shared": [True, False], "fPIC": [True, False] }

    # Provide default option values
    default_options = {"shared": False, "fPIC": True }

    # Define generators for this package
    generators = "cmake"

    # Setting no_copy_source to True prevents the source code from being copied
    # to the build area
    no_copy_source = True

    def requirements(self):
        # Specify requirements based on architecture. Generally speaking the gtest library
        # is not required for the armv8 build.
        if ( self.settings.arch == "x86_64" ):
            self.requires( "gtest/1.10.0" )
        if ( self.settings.arch == "armv8" ):
            pass

    def config_options(self):
        # Specify options in the configuration stage
        if self.settings.os == "Linux":
            del self.options.fPIC

    def source(self):
        # Obtain the source repository based on configured parameters and
        # checkout configured branch
        self.run( f"git clone { self.url } { self.source_folder } " )
        self.run( f"cd { self.source_folder } && git checkout { self.source_folder_branch } " )

    def configure_cmake_armv8(self,cmake):
        cmake.definitions["CMAKE_TOOLCHAIN_FILE"] = "aarch64.cmake"
        cmake.definitions["CMAKE_RELEASE_TYPE"] = "RELEASE"

    def build(self):
        # Build the package
        cmake = CMake(self)
        if ( self.settings.arch == "armv8" ):
            self.configure_cmake_armv8(cmake)
        # For some reason, the source_folder is not consistently set, depending
        # on whether the package is being built locally, or in the local cache.
        # Set this configure parameter explicitly if building in the local cache
        # to the name of the gitlab repo
        # Note that the self.in_local_cache variable is set by conan and only if
        # conan is building the package in the local cache.
        if ( self.in_local_cache ):
            cmake.configure( source_folder=f'{self.source_folder}/conan/{self.name}' )
        else:
            cmake.configure()
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        # Package the artifacts
        # The src and dst arguments refer to locations in the local cache that
        # are managed by conan.
        self.copy("*.h", src="include", dst="include", keep_path=False)
        self.copy("*.a", src="lib", dst="lib", keep_path=False)
        self.copy("test-*", src="bin", dst="bin", keep_path=False)

    def package_info(self):
        # Define supplemental package info if required.
        self.cpp_info.name = self.name
        self.cpp_info.libs = [ self.name ]

    def env_info(self):
        # Define any environment variables required by the package here.
        # self.env_info.path.append("CHECK")
        # self.env_info.newvariable = "VALUE"
        if ( self.arch.os == "armv8" ):
            self.env_info.CMAKE_TOOLCHAIN_FILE="aarch64.cmake"
